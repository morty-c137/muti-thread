package com.yc.learn.thread.methods;

/**
 * Package: com.yc.learn.thread.methods
 * <p>
 * Description： 实现Runnable的第二种方式：实现runnable
 * 这里利用的是lambda表达式，一句话作为执行语句，可以省略花括号
 * Author: wondefulMorty
 * <p>
 * Date: Created in 2022/1/1 21:07
 */
public class MyRunnable {
    /**
     *
     * @param args
     * 执行结果：
     * 主线程启动了....
     * 子线程实现runnable启动了....
     */
    public static void main(String[] args) {
        System.out.println("主线程启动了....");
        Runnable defRunnable = ()->System.out.println("子线程实现runnable启动了....");
        defRunnable.run();
    }
}

