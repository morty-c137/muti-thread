package com.yc.learn.thread.methods;

/**
 * Package: com.yc.learn.thread.methods
 * <p>
 * Description： 开启线程的方式1：
 *基础Thread类
 * Author: wondefulMorty
 * <p>
 * Date: Created in 2022/1/1 21:07
 */
public class MyThread {
    /**
     *
     * @param args
     * 打印结果：
     *主线程启动了...
     * 子线程启动了...
     *
     */
    public static void main(String[] args) {
        System.out.println("主线程启动了...");
        DefThread defThread = new DefThread();
        defThread.start();
    }

}

class DefThread extends  Thread{
    @Override
    public void run() {
        System.out.println("子线程启动了...");
    }
}
